package im.gitter.gitter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.volley.toolbox.ImageLoader;
import im.gitter.gitter.activities.MainActivity;
import im.gitter.gitter.models.Group;
import im.gitter.gitter.models.User;
import im.gitter.gitter.network.VolleySingleton;
import im.gitter.gitter.utils.AvatarUtils;
import im.gitter.gitter.utils.CircleNetworkImageView;
import im.gitter.gitter.utils.CursorUtils;
import im.gitter.gitter.utils.ThemeUtils;

public class DrawerAdapter extends RecyclerView.Adapter {

    private static final int HEADER_VIEW_TYPE = 0;
    private static final int NAVIGATION_ITEM_VIEW_TYPE = 1;
    private static final int COMMUNITY_SUBHEADING_VIEW_TYPE = 2;
    private static final int COMMUNITY_VIEW_TYPE = 3;
    private static final int DIVIDER_VIEW_TYPE = 4;
    private static final int VERSION_VIEW_TYPE = 5;

    private final ImageLoader imageLoader;
    private final AvatarUtils avatarUtils;
    private Cursor userCursor;
    private Cursor unreadCountsCursor;
    private Cursor userGroupsWithUnreadCountsCursor;
    private final String versionName;
    private Activity activity;

    public DrawerAdapter(Activity activity) {
        imageLoader = VolleySingleton.getInstance(activity).getImageLoader();
        avatarUtils = new AvatarUtils(activity);

        String versionName = "";
        try {
            versionName = "v" + activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        this.versionName = versionName;
        this.activity = activity;
    }

    @Override
    public int getItemCount() {
        int userGroupCount = getUserGroupCursorSize();
        return userGroupCount > 0 ? 7 + userGroupCount : 6;
    }

    @Override
    public int getItemViewType(int position) {
        int userGroupCount = getUserGroupCursorSize();
        int rowsAboveCommunities = userGroupCount > 0 ? 4 : 3;

        if (position == 0) {
            return HEADER_VIEW_TYPE;
        } else if (position == 3 && userGroupCount > 0) {
            return COMMUNITY_SUBHEADING_VIEW_TYPE;
        } else if ((position - rowsAboveCommunities) >= 0 && (position - rowsAboveCommunities) < userGroupCount) {
            return COMMUNITY_VIEW_TYPE;
        } else if (position == userGroupCount + rowsAboveCommunities) {
            return DIVIDER_VIEW_TYPE;
        } else if (position == userGroupCount + rowsAboveCommunities + 2) {
            return VERSION_VIEW_TYPE;
        } else {
            return NAVIGATION_ITEM_VIEW_TYPE;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case HEADER_VIEW_TYPE:
                return new HeaderViewHolder(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.drawer_header, parent, false));
            case NAVIGATION_ITEM_VIEW_TYPE:
                return new NavigationItemViewHolder(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.drawer_navigation_item, parent, false));
            case COMMUNITY_SUBHEADING_VIEW_TYPE:
                return new ViewHolder(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.drawer_subheader, parent, false));
            case COMMUNITY_VIEW_TYPE:
                return new CommunityViewHolder(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.drawer_community, parent, false));
            case DIVIDER_VIEW_TYPE:
                return new ViewHolder(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.drawer_divider, parent, false));
            case VERSION_VIEW_TYPE:
                return new VersionViewHolder(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.drawer_version, parent, false));
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder genericHolder, int position) {
        if (genericHolder instanceof HeaderViewHolder) {
            if (userCursor != null && userCursor.moveToFirst()) {
                User user = User.newInstance(CursorUtils.getContentValues(userCursor));
                HeaderViewHolder holder = (HeaderViewHolder) genericHolder;
                holder.avatarView.setImageUrl(avatarUtils.getAvatarWithDimen(user.getAvatarUrl(), R.dimen.drawer_header_avatar_size), imageLoader);
                holder.avatarView.setDefaultImageResId(R.drawable.default_avatar);
                holder.displayNameView.setText(user.getDisplayName());
                holder.usernameView.setText("@" + user.getUsername());
            }
        } else if (genericHolder instanceof NavigationItemViewHolder) {
            NavigationItemViewHolder holder = (NavigationItemViewHolder) genericHolder;
            if (position == 1) {

                holder.imageView.setImageResource(R.drawable.ic_home_white_24dp);
                holder.titleView.setText(R.string.all_conversations);
                if (unreadCountsCursor != null && unreadCountsCursor.moveToFirst()) {
                    holder.badgeView.setBadge(
                            CursorUtils.getInt(unreadCountsCursor, "unreadRooms"),
                            CursorUtils.getInt(unreadCountsCursor, "mentionedRooms")
                    );
                } else {
                    holder.badgeView.setBadge(0, 0);
                }
                holder.intentKey = MainActivity.GO_TO_ALL_CONVERSATIONS_INTENT_KEY;
            } else if (position == 2) {
                holder.imageView.setImageResource(R.drawable.ic_person_black_24dp);
                holder.titleView.setText(R.string.people);
                if (unreadCountsCursor != null && unreadCountsCursor.moveToFirst()) {
                    holder.badgeView.setBadge(
                            CursorUtils.getInt(unreadCountsCursor, "unreadOneToOnes"),
                            CursorUtils.getInt(unreadCountsCursor, "mentionedOneToOnes")
                    );
                } else {
                    holder.badgeView.setBadge(0, 0);
                }
                holder.intentKey = MainActivity.GO_TO_ALL_PEOPLE_INTENT_KEY;
            } else {
                holder.imageView.setImageResource(R.drawable.ic_exit_to_app_white_24dp);
                holder.titleView.setText(R.string.signout);
                holder.badgeView.setBadge(0, 0);
                holder.intentKey = MainActivity.SIGN_OUT_INTENT_KEY;
            }
        } else if (genericHolder instanceof CommunityViewHolder) {
            onBindCommunityViewHolder((CommunityViewHolder) genericHolder, position - 4);
        } else if (genericHolder instanceof VersionViewHolder) {
            VersionViewHolder holder = (VersionViewHolder) genericHolder;
            holder.versionTextView.setText(versionName);
        }
    }

    private int getUserGroupCursorSize() {
        return userGroupsWithUnreadCountsCursor != null ? userGroupsWithUnreadCountsCursor.getCount() : 0;
    }

    private void onBindCommunityViewHolder(CommunityViewHolder holder, int relativePosition) {
        userGroupsWithUnreadCountsCursor.moveToPosition(relativePosition);
        Group group = Group.newInstance(CursorUtils.getContentValues(userGroupsWithUnreadCountsCursor));
        int unreads = CursorUtils.getInt(userGroupsWithUnreadCountsCursor, "unreadRooms");
        int mentions = CursorUtils.getInt(userGroupsWithUnreadCountsCursor, "mentionedRooms");

        holder.imageView.setImageUrl(avatarUtils.getAvatarWithDimen(group.getAvatarUrl(), R.dimen.drawer_item_avatar_size), imageLoader);
        holder.imageView.setDefaultImageResId(R.drawable.default_avatar);
        holder.titleView.setText(group.getName());
        holder.badgeView.setBadge(unreads, mentions);
        holder.groupId = group.getId();
    }

    public void setUser(Cursor userCursor) {
        if (this.userCursor != userCursor) {
            this.userCursor = userCursor;
            notifyItemChanged(0);
        }
    }

    public void setUnreadCounts(Cursor unreadCountsCursor) {
        if (this.unreadCountsCursor != unreadCountsCursor) {
            this.unreadCountsCursor = unreadCountsCursor;
            notifyItemChanged(1);
            notifyItemChanged(2);
        }
    }

    public void setUserGroupsWithUnreadCounts(Cursor userGroupsWithUnreadCountsCursor) {
        if (this.userGroupsWithUnreadCountsCursor != userGroupsWithUnreadCountsCursor) {
            this.userGroupsWithUnreadCountsCursor = userGroupsWithUnreadCountsCursor;
            notifyDataSetChanged();
        }
    }

    private class HeaderViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        final CircleNetworkImageView avatarView;
        final TextView displayNameView;
        final TextView usernameView;
        final ImageView switchThemeView;

        HeaderViewHolder(View headerView) {
            super(headerView);
            avatarView = (CircleNetworkImageView) headerView.findViewById(R.id.user_avatar);
            displayNameView = (TextView) headerView.findViewById(R.id.user_display_name);
            usernameView = (TextView) headerView.findViewById(R.id.user_username);
            switchThemeView = (ImageView) headerView.findViewById(R.id.switch_theme);
            switchThemeView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.switch_theme) {
                int currentThemeMode = AppCompatDelegate.getDefaultNightMode();

                int newThemeMode = AppCompatDelegate.MODE_NIGHT_NO;
                if (currentThemeMode == AppCompatDelegate.MODE_NIGHT_NO) {
                    newThemeMode = AppCompatDelegate.MODE_NIGHT_YES;
                }

                AppCompatDelegate.setDefaultNightMode(newThemeMode);
                new ThemeUtils(activity).saveThemeMode(newThemeMode);

                activity.recreate();
            }
        }
    }

    private static class NavigationItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        final ImageView imageView;
        final TextView titleView;
        final BadgeView badgeView;
        String intentKey;

        NavigationItemViewHolder(View view) {
            super(view);
            imageView = (ImageView) view.findViewById(R.id.image);
            titleView = (TextView) view.findViewById(R.id.title);
            badgeView = (BadgeView) view.findViewById(R.id.badge);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (intentKey != null) {
                Context context = itemView.getContext();
                Intent intent = new Intent(context, MainActivity.class);
                intent.putExtra(intentKey, true);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                context.startActivity(intent);
            }
        }
    }

    private static class CommunityViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        final CircleNetworkImageView imageView;
        final TextView titleView;
        final BadgeView badgeView;
        String groupId;

        CommunityViewHolder(View view) {
            super(view);
            imageView = (CircleNetworkImageView) view.findViewById(R.id.image);
            titleView = (TextView) view.findViewById(R.id.title);
            badgeView = (BadgeView) view.findViewById(R.id.badge);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (groupId != null) {
                Context context = itemView.getContext();
                Intent intent = new Intent(context, MainActivity.class);
                intent.putExtra(MainActivity.GO_TO_GROUP_ID_INTENT_KEY, groupId);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                context.startActivity(intent);
            }
        }
    }

    private class VersionViewHolder extends RecyclerView.ViewHolder {

        final TextView versionTextView;

        VersionViewHolder(View itemView) {
            super(itemView);
            versionTextView = (TextView) itemView.findViewById(R.id.app_version);
        }
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
